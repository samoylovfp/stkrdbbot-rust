use serde_json::{Value, from_value};
use types::Result;
use reqwest::Client;


const GET_UPDATES_TIMEOUT: u32 = 30;


pub trait Network {
    fn post(&self, url: &str, body: Value) -> Result<Value>;
}

pub struct RealNetwork {
    client: Client
}

impl RealNetwork {
    pub fn new() -> Result<Self> {
        Ok(RealNetwork{
            client: Client::new()?
        })
    }
}

impl Network for RealNetwork {
    fn post(&self, url: &str, body: Value) -> Result<Value> {
        let response = self.client.post(url)?.json(&body)?.send()?.json()?;
        Ok(response)
    }
}

pub struct Api<N: Network=RealNetwork> {
    network: N,
    last_update_id: Option<u64>,
    api_key: String
}

impl<N: Network> Api<N> {
    pub fn new<T: Into<String>>(network: N, api_key: T) -> Result<Api<N>> {
        Ok(Api{
            network,
            api_key: api_key.into(),
            last_update_id: None
        })
    }

    pub fn get_updates(&mut self) -> Result<Vec<Update>> {
        let mut params = json!({
            "timeout": GET_UPDATES_TIMEOUT,
            "allowed_updates": ["message", "inline_query"]
        });
        if let Some(update_id) = self.last_update_id {
            params.as_object_mut().unwrap().insert("offset".into(), (update_id + 1u64).into());
        }
        let mut response = self.network.post(&format!("https://api.telegram.org/bot{}/getUpdates", self.api_key), params)?;
        let response_value: Value = response.as_object_mut().and_then(|o| o.remove("result")).ok_or(format!("Invalid object: {:?}", response))?;
        let result: Vec<super::tg_types::TgUpdate> = from_value(response_value)?;
        Ok(result.into_iter().map(|u| {
            match self.last_update_id {
                None => self.last_update_id = Some(u.update_id),
                Some(update_id) => if u.update_id > update_id {self.last_update_id = Some(u.update_id)}
            };
            match u.message {
                Some(ref msg) => msg.text.as_ref().map(|t| Update::Message {
                    chat_id: msg.chat.id,
                    text: t.clone(),
                    username: msg.chat.username.clone()
                }).or_else(|| msg.sticker.as_ref().map(|s| Update::Sticker {
                    file_id: s.file_id.clone(),
                    chat_id: msg.chat.id,
                    sticker_set: s.set_name.clone()
                })),
                None => u.inline_query.as_ref().map(|q| Update::InlineQuery{
                    id: q.id.clone(),
                    offset: q.offset.clone(),
                    text: q.query.clone()
                })
            }.unwrap_or_else(|| {warn!("Unknown update {:?}", &u); Update::Unknown})
        }).collect())
    }

    pub fn answer_inline_query_with_stickers(&self, query_id: String, sticker_ids: Vec<String>) -> Result<()> {
        let sticker_results: Vec<Value> = sticker_ids.into_iter().map(|sid| json!({
            "type": "sticker",
            "id": sid,
            "sticker_file_id": sid
        })).collect();
        self.network.post(
            &format!("https://api.telegram.org/bot{}/answerInlineQuery", self.api_key),
            json!({
                "inline_query_id": query_id,
                "results": sticker_results
            }))?;
        Ok(())
    }

    pub fn send_message(&self, chat_id: u64, text: String, keyboard: Option<Value>) -> Result<()> {
        let mut response = json!({
            "chat_id": chat_id,
            "text": text
        });
        if let Some(kb) = keyboard {
            response.as_object_mut().unwrap().insert("reply_markup".into(), kb);
        }

        self.network.post(
            &format!("https://api.telegram.org/bot{}/sendMessage", self.api_key),
            response
        )?;
        Ok(())
    }
}

impl Api<RealNetwork> {

    pub fn with_real_network<T: Into<String>>(api_key: T) -> Result<Api<RealNetwork>> {
        Ok(Api {
            network: RealNetwork::new()?,
            api_key: api_key.into(),
            last_update_id: None
        })
    }
}

#[derive(Debug, PartialEq)]
pub enum Update {
    Message {
        text: String,
        chat_id: u64,
        username: Option<String>
    },
    Sticker {
        file_id: String,
        chat_id: u64,
        sticker_set: Option<String>
    },
    InlineQuery {
        id: String,
        offset: String,
        text: String
    },
    Unknown
}


#[cfg(test)]
mod test{
    use super::*;
    use std::cell::RefCell;
    use std::collections::vec_deque::VecDeque;
    struct FakeNetwork {
        dialog: RefCell<VecDeque<(String, Value, Result<Value>)>>
    }

    impl FakeNetwork {
        fn new() -> FakeNetwork {
            FakeNetwork{
                dialog: RefCell::new(VecDeque::new())
            }
        }
        fn expect<I: Into<String>>(&self, url: I, request: Value, response: Result<Value>) {
            self.dialog.borrow_mut().push_front((url.into(), request, response))
        }
    }

    impl Drop for FakeNetwork {
        fn drop(&mut self) {
            if ::std::thread::panicking() {
                return
            }
            let calls_left = self.dialog.borrow();
            if calls_left.len() > 0 {
                panic!("Expected calls not called {:?}", calls_left);
            }
        }
    }

    impl Network for FakeNetwork {
        fn post(&self, url: &str, body: Value) -> Result<Value> {
            let (expected_url, expected_request, response) = self.dialog.borrow_mut().pop_back()
            .expect(&format!("Unexpected call to {} {:?}", url, body));
            assert_eq!(url, &expected_url);
            assert_eq!(body, expected_request);
            response
        }
    }

    #[test]
    fn test_get_updates() {
        let n = FakeNetwork::new();
        n.expect("https://api.telegram.org/bot__token__/getUpdates",
            json!({"timeout":30, "allowed_updates": ["message", "inline_query"]}),
            Ok(json!({"ok":true,"result":[{"update_id":430138284, "message":{"message_id":150,"from":{"id":123123,"is_bot":false,"first_name":"F","last_name":"L","username":"username","language_code":"en-US"},"chat":{"id":1231234,"first_name":"F","last_name":"L","username":"username","type":"private"},"date":1504348147,"text":"ukh"}}]}))
        );
        n.expect("https://api.telegram.org/bot__token__/getUpdates",
            json!({"timeout":30, "allowed_updates": ["message", "inline_query"], "offset": 430138285}),
            Ok(json!({"ok":true,"result":[{"update_id":430138285, "message":{"message_id":150,"from":{"id":123123,"is_bot":false,"first_name":"F","last_name":"L","username":"username","language_code":"en-US"},"chat":{"id":1231234,"first_name":"F","last_name":"L","username":"username","type":"private"},"date":1504348147,"text":"azaza"}}]}))
        );
        let mut api = Api::new(n, "__token__").unwrap();
        assert_eq!(api.get_updates().unwrap(), vec![Update::Message{
            chat_id: 1231234,
            username: Some("username".into()),
            text: "ukh".into()
        }]);
        assert_eq!(api.get_updates().unwrap(), vec![Update::Message{
            chat_id: 1231234,
            username: Some("username".into()),
            text: "azaza".into()
        }]);
    }
}