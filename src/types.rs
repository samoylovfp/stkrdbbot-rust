error_chain! {
    foreign_links {
        Db(::rusqlite::Error);
        Serde(::serde_json::error::Error);
        Api(::reqwest::Error);
    }
}
