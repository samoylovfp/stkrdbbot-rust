CREATE TABLE tags (
    name TEXT,
    lower TEXT UNIQUE on conflict ignore,
    tag_id INTEGER PRIMARY KEY
);

CREATE TABLE stickers (
    id INTEGER PRIMARY KEY,
    sticker_set TEXT
);

CREATE TABLE sticker_file_ids (
    file_id TEXT,
    sticker_id INTEGER,
    FOREIGN KEY (sticker_id) REFERENCES stickers(id),
    UNIQUE (file_id, sticker_id) ON CONFLICT IGNORE
);

CREATE TABLE users (
    tg_user_id TEXT UNIQUE ON CONFLICT IGNORE,
    login TEXT UNIQUE ON CONFLICT IGNORE
);

CREATE TABLE assignments (
    tag_id INTEGER,
    sticker_id INTEGER,
    user_id TEXT,
    timestamp INTEGER,
    FOREIGN KEY (user_id) REFERENCES users(id),
    FOREIGN KEY (sticker_id) REFERENCES sticker_file_ids(sticker_id),
    FOREIGN KEY (tag_id) REFERENCES tags(id),
    UNIQUE (tag_id, sticker_id) ON CONFLICT IGNORE
);
